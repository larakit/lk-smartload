<?php
/**
 * Created by PhpStorm.
 * User: koksharov
 * Date: 17.10.16
 * Time: 9:58
 */

namespace Larakit\SmartLoad;

class SmartLoad {

    static function response($parts, $layout = '!.layouts.default') {
        if(\Request::ajax()) {
            $ret['result'] = 'success';
            $ret['parts']  = $parts;

            return $ret;
        } else {
            return \LaraPage::setContent(view($layout, $parts));
        }
    }
}