LarakitJs.initSelector('js-smartload', function () {
    $(this).on('click', function (data) {
        var selector;
        if ('success' == data.result) {
            $.each(data.parts, function(k, v){
                selector = '.js-' + k;
                $(selector).html(v);
            });
            $.each(data.classes.remove, function(k, v){
                selector = '.js-' + k;
                $(selector).removeClass(v);
            });
            $.each(data.classes.add, function(k, v){
                selector = '.js-' + k;
                $(selector).addClass(v);
            });
        }
    });
});